import { Mode, Command, Protocol } from './protocol';
import * as _ from "lodash";

export default class extends Protocol {

    constructor(port, opt = {}) {
        opt = Object.assign({
            rate: 31250,
            // enumerateOnConnect: true
        }, opt);

        if (typeof port === 'string')
            port = SerialPort(port, { baudRate: opt.rate });

        super(opt);

        this.settings = {};
        this.register(port);

        /*if (this.options.enumerateOnConnect) {
            this.once('pong', e => {
                if (! this.has(e))
                    return;

            });
        }*/
    }

    capabilities(device = 0xFF) {
        return new Promise((resolve, reject) => {
            if (! this.socket || ! this.socket.isOpen) {
                reject(Error('Unable to contact device, no connection'));
            }

            this.once('settings.response', e => {
                // If this response isn't for us, ignore it
                if (! this.has(e))
                    return;

                let offset = 0;
                let settings = {
                    banks: e.data.readInt8(offset++),
                    patches: e.data.readInt8(offset++),
                    default_bank: e.data.readInt8(offset++),
                    default_patch: e.data.readInt8(offset++),
                    max_length: e.data.readInt8(offset++),
                    max_glyphs: e.data.readInt8(offset),
                };

                this.settings = settings;

                resolve(settings);
            });

            this.send(Command.SETTINGS_QUERY, device);
        });
    }

    async configuration() {
        let banks = [];

        for (let b of _.range(0, this.settings.banks)) {
            let bank = {
                index: b,
                patches: []
            };

            for (let p of _.range(0, this.settings.patches)) {
                let patch = await this.patch(b, p);
                bank.patches.push(patch);
            }

            banks.push(bank);
        }

        return banks;
    }

    patch(bank, id) {
        return new Promise((resolve, reject) => {
            let cb = e => {
                let offset = 0;
                let bi = e.data.readUInt8(offset++);
                let p = e.data.readInt8(offset++);

                // If these aren't the droids we're looking for, skip the response
                if (bi !== bank && p !== id)
                    return;

                // Create the patch object
                let patch = {
                    index: p,
                    name: e.data.slice(offset, offset += this.settings.max_length).toString('ascii')
                };

                // Read channels
                patch.channels = e.data.readUInt16BE(offset);
                patch.activeColor = '#' + e.data.slice(offset + 2, offset += 5).toString('hex');
                patch.inactiveColor = '#' + e.data.slice(offset, offset + 3).toString('hex');

                // Remove this listener since our task is done
                this.removeListener('patch.response', cb);
                // this.removeAllListeners('patch.response');

                resolve(patch);
            };

            this.on('patch.response', cb);

            // Request the patch
            this.start(Command.PATCH_QUERY);
            this.write(bank);
            this.write(id);
            this.end();
        });
    }

    destroy() {
        if (this.port && this.port.isOpen)
            this.port.close();

        this.removeAllListeners();
    }
}

/* async loadBank(id) {
                return new Promise((resolve, reject) => {
                    com.on('bank.response', e => {
                        let index = e.data.readUInt8();
                        // If the message isn't for us or wrong bank ignore
                        if (! com.has(e) || index)
                            return;

                        let bank = {
                            index,
                            name: '',
                            patches: []
                        };

                        resolve(bank);
                    });
                });
            },*/