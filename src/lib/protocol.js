const EventEmitter = require('events').EventEmitter;

export const Mode = {
    RUN: 0,
    SOCKET: 1,
    PROGRAM: 2,
    PROGRAM_SERIAL: 3,
};

// 0xF0 and 0xF7 are reserved
export const Command = {
    // Modes
    MODE_SET: {
        key: 0x01,
        name: 'mode.set'
    },
    MODE_QUERY: {
        key: 0x02,
        name: 'mode.query'
    },
    MODE_RESPONSE: {
        key: 0x03,
        name: 'mode.response'
    },

    // Settings
    SETTINGS_QUERY: {
        key: 0x11,
        name: 'settings.query'
    },
    SETTINGS_RESPONSE: {
        key: 0x12,
        name: 'settings.response'
    },

    PATCH_SET: {
        key: 0x21,
        name: 'patch.set'
    },
    PATCH_QUERY: {
        key: 0x22,
        name: 'patch.query'
    },
    PATCH_RESPONSE: {
        key: 0x23,
        name: 'patch.response'
    },

    // System
    PING: {
        key: 0xFA,
        name: 'ping'
    },
    PONG: {
        key: 0xFB,
        name: 'pong'
    },
    // 0xFC, 0xFD, 0xFE are reserved
    // RESET: {
    //     key: 0xFF,
    //     name: 'reset'
    // }
};

export class Protocol extends EventEmitter {

    constructor(opts) {
        super();

        this.mapping = {};
        /**
         *
         * @type SerialPort
         */
        this.socket = null;
        this.timer = null;
        this.pong = false;

        this.options = Object.assign({
            timeout: 5000,
            prefix: [0x7D],
            id: 0x20
        }, opts);

        // Create a buffer from the prefix
        this.options.prefix = Buffer.from(this.options.prefix);

        for (let c in Command) {
            this.map(Command[c].key, Command[c].name);
        }

        console.debug('mapping', this.mapping);
    }

    map(key, name) {
        this.mapping[key] = name;
    }

    register(socket) {
        this.socket = socket;

        let parser = new SysexParser();

        this.socket.pipe(parser);

        parser.on('data', data => {
            this.process(data);
        }) ;

        parser.on('end', () => {
            clearTimeout(this.timer);
            this.timer = null;
            console.log('end stream');
        });

        this.on('ping', e => {
            this.send(Command.PONG);
        });

        this.on('pong', e => {
            this.pong = true;
        });

        this.on('timeout', () => {
            console.warn('pong not received');
        });

        this.ping();
    }

    process(data) {
        // If there is a prefix (AKA manufacturer ID)
        let prefix = data.slice(0, this.options.prefix.length);
        // Prefix required?
        if (this.options.prefix.length > 0 && ! prefix.equals(this.options.prefix)) {
            // This message isn't for us ignore it
            console.warn('received message for another vendor, ignored');
            return;
        }

        // The byte after is the command
        let command = data.slice(this.options.prefix.length, this.options.prefix.length + 1)[0] || null;
        let source = data.slice(this.options.prefix.length + 1, this.options.prefix.length + 2)[0] || null;
        let destination = data.slice(this.options.prefix.length + 2, this.options.prefix.length + 3)[0] || null;

        // The remaining data is next
        data = data.slice(this.options.prefix.length + 3);

        // Has a registered mapping?
        let event = this.mapping[command] || command;

        // Create the event object
        data = {
            event: event,
            command: command,
            source: source,
            dest: destination,
            data: data
        };

        console.debug('event', data);

        if (event) {
            this.emit(event, data);
        }
    }

    has(e) {
        return e.dest === 0xFF || e.dest === 0x00 || e.dest === this.options.id;
    }

    start(command, dest = 0xFF) {
        this.write(0xF0);
        this.write(this.options.prefix);

        if (typeof command === 'object') {
            this.write(command.key);
        } else {
            this.write(command);
        }

        // Source Device
        this.write(this.options.id);
        // Destination Device
        this.write(dest);
    }

    end() {
        this.write(0xF7);
    }

    send(command, device = 0xFF, data = []) {
        this.start(command, device);
        this.sendData(data);
        this.end();
    }

    sendData(data) {
        console.debug('sendData', data);
        if (data && data.length > 0) {
            for (let byte of data) {
                this.write(byte);
            }
        }
    }

    write(data) {
        if (! this.socket) {
            console.error('Tried to send data but no socket is open');

            return;
        }

        if (! Buffer.isBuffer(data) && ! Array.isArray(data)) {
            data = [data];
        }

        this.socket.write(data);
    }

    ping() {
        this.pong = false;
        this.send(Command.PING);

        this.timer = setTimeout(() => {
            // no response was received
            if (! this.pong) {
                this.emit('timeout');
            }

            this.ping();
        }, this.options.timeout);
    }
}