import setters from './setters'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

export default {
    setters,
    getters,
    actions,
    mutations,
    state: {
        state: 'disconnected',
        loading: false,
        config: {
            settings: {},
            banks: []
        },
        device: null,
        controller: null,
    }
}