import fs from 'fs';

export default {
    loadConfigFromFile(state, path) {
        state.config = JSON.parse(fs.readFileSync(path));
    }
}