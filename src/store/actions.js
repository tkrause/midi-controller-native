export default {
    loadConfigFile({ commit }, path) {
        commit('loadConfigFromFile', path);
    }
}