import PortSelection from './pages/PortSelection.vue'
import Device from './pages/Device.vue'
import EditPatch from './pages/EditPatch.vue'

export default [
    {
        path: '/',
        component: PortSelection,
        name: 'select'
    },
    {
        path: '/device/:port',
        component: Device,
        meta: {
            title: '{:port}'
        },
        name: 'device',
        children: [
            {
                path: 'patch/:id',
                name: 'edit.patch',
                component: EditPatch,
                meta: {
                    title: 'Edit Patch {:id}'
                }
            }
        ]
    }
]