'use strict';
const Buffer = require('safe-buffer').Buffer;
const Transform = require('stream').Transform;

module.exports = class SysexParser extends Transform {
    constructor(options) {
        options = options || {
            start: 0xF0,
            end: 0xF7
        };

        super(options);

        this.buffer = Buffer.alloc(0);

        this.capturing = false;
        this.start = options.start;
        this.end = options.end;
    }

    _transform(chunk, encoding, cb) {

        for (let byte of chunk) {
            // If we find a start block, start capturing
            // otherwise skip these bytes
            if (! this.capturing ) {
                this.capturing = byte === this.start;

                continue;
            }

            // Assume we're capturing
            if (byte === this.end) {
                this.capturing = false;
                // Push the entire buffer and reset it
                this.sendBuffer();

                continue;
            }

            let data = Buffer.from([byte]);
            this.buffer = Buffer.concat([this.buffer, data]);
        }

        cb();
    }

    _flush(cb) {
        if (this.capturing) {
            this.capturing = false;
            this.sendBuffer();
        }

        cb();
    }

    sendBuffer() {
        this.push(this.buffer);
        this.buffer = Buffer.alloc(0);
    }
};
