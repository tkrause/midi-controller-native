import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import 'bootstrap'
import 'lodash'

import Window from './App.vue'
import Loader from './components/Loader.vue'
import Titlebar from './components/Titlebar.vue'
import Toolbar from './components/Toolbar.vue'
import Switch from 'vue-switches'

Vue.component('window', Window);
Vue.component('loader', Loader);
Vue.component('titlebar', Titlebar);
Vue.component('toolbar', Toolbar);
Vue.component('switcher', Switch);

Vue.use(VueRouter);
Vue.use(Vuex);

Vue.filter('trim', v => {
    if (! v) return '';

    return v.trimLeft().trimRight();
});

Vue.filter('print', v => {
    if (! v) return '';

    return v.replace(/[^\x20-\x7E]+/g, '');
});

Vue.filter('placeholder', (v, p) => {
    return v || p;
});

import routes from './routes'
const router = new VueRouter({ routes });
router.beforeEach((to, from, next) => {
    if (to.matched.length <= 0) {
        // alert('Invalid operation');
        next(false);
    } else {
        next();
    }
});

import VueStore from './store/store'
const store = new Vuex.Store(VueStore);

const app = new Vue({
    el: '#app',
    router,
    store
    // render: h => h(App)
});

/*
    data: {
        loading: false,
        color: null,
        ports: [],
        port: null
    },
    methods: {
        selectPort(path) {
            console.log(path);
        },
        async enumeratePorts() {
            this.loading = true;
            let ports = await SerialPort.list();

            for (let i = 0; i < ports.length; i++) {
                let available = false;

                try {
                    let p = await this.openPort(ports[i].comName);
                    await this.closePort(p);

                    available = true;
                } catch (e) {
                    console.log(e);
                }

                ports[i].available = available;
            }

            this.ports = ports;
        },
        async openPort(path) {
            return new Promise((resolve, reject) => {
                let port = SerialPort(path, { autoOpen: false });
                port.open(err => {
                    ! err ? resolve(port) : reject(err);
                });
            });
        },
        async closePort(port) {
            return new Promise((resolve, reject) => {
                port.close(err => {
                    resolve(err);
                });
            });
        }
    },
    created() {
        this.enumeratePorts();
    }
});*/