const {app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const { lstatSync, readdirSync } = require('fs');
const url = require('url');

if (process.env.NODE_ENV === 'development') {
    require('electron-reload')(__dirname);
}

if (process.env.NODE_ENV === 'development') {
    require('electron-context-menu')();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const isDirectory = source => lstatSync(source).isDirectory();
const getDirectories = source => readdirSync(source).map(name => path.join(source, name)).filter(isDirectory);

function createWindow () {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        frame: false,
        title: "MIDI Controller",
        // backgroundColor: '#221F2A'
        // radii: [10, 10, 10, 10]
    });
    // and load the index.html of the app.
    let startUrl = url.format({
        pathname: path.join(__dirname, 'app/index.html'),
        protocol: 'file:',
        slashes: true
    });

    // If we're in development
    if (process.env.NODE_ENV === 'development') {
        // startUrl = process.env.ELECTRON_START_URL || startUrl;

        // Open the DevTools.
        mainWindow.webContents.openDevTools({ mode: 'detach' });

        require('devtron').install();

        let extDir = path.join(process.env.HOMEPATH, '/AppData/Local/Google/Chrome/User Data/Default/Extensions/nhdogjmejiglipccpnnnanhbledajbpd');
        console.log('Scanning', extDir, 'for extensions...');

        for (let ext of getDirectories(extDir)) {
            console.log('Found and loading', ext);
            BrowserWindow.addDevToolsExtension(ext);
        }
    }

    mainWindow.loadURL(startUrl);

    // Emitted when the window is closed.
    mainWindow.on('closed', () => {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null
    })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow()
    }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
