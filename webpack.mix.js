const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('dist')
    .js('src/main.js', 'dist/js/app.js')
    .sass('resources/scss/app.scss', 'dist/css/app.css')
    .copy('node_modules/font-awesome/fonts', 'dist/fonts')
    .webpackConfig(webpack => {
        return {
            target: 'electron-renderer',
            plugins: [
                new webpack.ProvidePlugin({
                    $: 'jquery',
                    jQuery: 'jquery',
                    'window.jQuery': 'jquery',
                    Popper: ['popper.js', 'default'],
                }),
                new webpack.DefinePlugin({
                    APP_NAME: JSON.stringify('MIDI Controller')
                })
            ]
        };
    });
// .extract(['vue','jquery','bootstrap'])
//     .sass('resources/assets/sass/app.scss', 'dist/css')
//     .sass('resources/assets/sass/template/style_dark.scss', 'dist/css/main.css')
//     .sass('resources/assets/sass/template/icons.scss', 'dist/css/icons.css');